class VastBuilder
  AD_SYSTEM = "SerialVastGenerator"

  def initialize(params)
    @params      = params
    @media_files = params[:media_files]
    @icons       = params[:icons]
  end

  def build
    xml = <<~"XML"
<VAST version="4.2" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.iab.com/VAST">
  <Ad id="1" >
    <InLine>
      <AdSystem version="1">#{AD_SYSTEM}</AdSystem>
      <Error><![CDATA[https://example.com/error]]></Error>
      <Impression id="Impression-ID"><![CDATA[https://example.com/track/impression]]></Impression>
      <Pricing model="cpm" currency="USD">
        <![CDATA[ 25.00 ]]>
      </Pricing>
      <AdServingId>00000000-0000-0000-0000-000000000000</AdServingId>
      <AdTitle>#{@params[:ad_title]}</AdTitle>
      <Creatives>
        <Creative id="1" sequence="1" adId="1">
          <Linear>
            <Duration>#{@params[:duration]}</Duration>
#{build_media_files if @media_files.present? }
            <VideoClicks>
              <ClickThrough />
            </VideoClicks>
#{build_icons if @icons.present? }
          </Linear>
        </Creative>
      </Creatives>
    </InLine>
  </Ad>
</VAST>
    XML
  end

  private

  def build_media_files
    ins = @media_files.map do |h|
      <<-"XML".chomp
              <MediaFile id="1" delivery="#{h[:delivery]}" type="#{h[:type]}" bitrate="#{h[:bitrate]}" width="#{h[:width]}" height="#{h[:height]}" minBitrate="#{h[:min_bitrate]}" maxBitrate="#{h[:max_bitrate]}" scalable="#{h[:scalable]}" maintainAspectRatio="#{h[:maintain_aspect_ratio]}" codec="#{h[:codec]}">
                <![CDATA[#{h[:url]}]]>
              </MediaFile>
      XML
    end

    <<-"XML".chomp
            <MediaFiles>
#{ins.join('\n')}
            </MediaFiles>
    XML
  end

  def build_icons
    ins = @icons.map do |h|
      <<-"XML".chomp
                <Icon program="#{h[:program]}" height="#{h[:height]}" width="#{h[:width]}" xPosition="#{h[:x_position]}" yPosition="#{h[:y_position]}">
                  <StaticResource creativeType="#{h[:type]}">
                    <![CDATA[#{h[:url]}]]>
                  </StaticResource>
                  <IconClicks>
                    <IconClickThrough />
                  </IconClicks>
                </Icon>
      XML
    end

    <<-"XML".chomp
            <Icons>
#{ins.join('\n')}
            </Icons>
    XML
  end
end
