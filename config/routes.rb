Rails.application.routes.draw do
  resources :creatives do
    member do
      get 'vast_xml', format: 'xml'
    end
  end

  get 'vast_xml/sample_icon_base_text', format: 'xml'
  get 'vast_xml/sample_video_base_text', format: 'xml'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
