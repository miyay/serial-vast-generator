class AddUseOverlayTextToIconStructure < ActiveRecord::Migration[7.0]
  def change
    add_column :icon_structures, :use_overlay_text, :boolean, default: false, null: false
    add_column :icon_structures, :default_text, :string
  end
end
