class CreateMediaFileGroups < ActiveRecord::Migration[7.0]
  def change
    create_table :media_file_groups do |t|
      t.belongs_to :linear_structure, null: false, foreign_key: true
      t.float :duration
      t.string :video_upload_format

      t.timestamps
    end

    remove_column :linear_structures, :duration, :float
    remove_column :linear_structures, :video_upload_format, :string
  end
end
