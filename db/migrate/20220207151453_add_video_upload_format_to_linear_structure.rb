class AddVideoUploadFormatToLinearStructure < ActiveRecord::Migration[7.0]
  def change
    add_column :linear_structures, :video_upload_format, :string
  end
end
