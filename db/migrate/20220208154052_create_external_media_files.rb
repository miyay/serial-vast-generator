class CreateExternalMediaFiles < ActiveRecord::Migration[7.0]
  def change
    create_table :external_media_files do |t|
      t.belongs_to :media_file_group, null: false, foreign_key: true
      t.string :external_host_url
      t.string :mime_type
      t.integer :width
      t.integer :height
      t.integer :bitrate
      t.string :codec
      t.integer :file_size
      t.float :duration

      t.timestamps
    end

    drop_table :media_file_structures do |t|
      t.belongs_to :linear_structure, null: false, foreign_key: true
      t.string :external_host_url
      t.string :mime_type
      t.integer :width
      t.integer :height
      t.integer :bitrate
      t.string :codec
      t.integer :file_size
      t.float :duration

      t.timestamps
    end
  end
end
