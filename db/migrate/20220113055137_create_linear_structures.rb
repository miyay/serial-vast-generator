class CreateLinearStructures < ActiveRecord::Migration[7.0]
  def change
    create_table :linear_structures do |t|
      t.belongs_to :vast_structure, null: false, foreign_key: true
      t.float :duration

      t.timestamps
    end
  end
end
