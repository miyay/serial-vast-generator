class AddOriginalMediaFileToMediaFileGroup < ActiveRecord::Migration[7.0]
  def change
    add_column :media_file_groups, :original_media_file, :string
  end
end
