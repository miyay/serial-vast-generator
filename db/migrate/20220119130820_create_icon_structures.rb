class CreateIconStructures < ActiveRecord::Migration[7.0]
  def change
    create_table :icon_structures do |t|
      t.belongs_to :linear_structure, null: false, foreign_key: true
      t.string :icon_image
      t.string :program
      t.integer :width
      t.integer :height
      t.string :x_position
      t.string :y_position
      t.integer :offset
      t.integer :duration
      t.string :mime_type

      t.timestamps
    end
  end
end
