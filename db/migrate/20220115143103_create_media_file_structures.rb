class CreateMediaFileStructures < ActiveRecord::Migration[7.0]
  def change
    create_table :media_file_structures do |t|
      t.belongs_to :linear_structure, null: false, foreign_key: true
      t.string :external_host_url
      t.string :mime_type
      t.integer :width
      t.integer :height
      t.integer :bitrate
      t.string :codec
      t.integer :file_size
      t.float :duration

      t.timestamps
    end
  end
end
