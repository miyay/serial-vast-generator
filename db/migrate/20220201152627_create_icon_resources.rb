class CreateIconResources < ActiveRecord::Migration[7.0]
  def change
    create_table :icon_resources do |t|
      t.belongs_to :icon_structure, null: false, foreign_key: true
      t.string :format
      t.string :icon_file
      t.string :mime_type
      t.integer :width
      t.integer :height
      t.jsonb :overlay_data, null: false, default: {}
      t.jsonb :qr_data, null: false, default: {}

      t.timestamps
    end

    remove_column :icon_structures, :icon_image, :string
    remove_column :icon_structures, :mime_type, :string
    remove_column :icon_structures, :use_overlay_text, :boolean, default: false, null: false
    remove_column :icon_structures, :default_text, :string
    remove_column :icon_structures, :width, :integer
    remove_column :icon_structures, :height, :integer
  end
end
