class AddConvertOptionToMediaFileGroups < ActiveRecord::Migration[7.0]
  def change
    add_column :media_file_groups, :convert_option, :jsonb, null: false, default: {}
    add_column :media_file_groups, :overlay_image_file, :string
  end
end
