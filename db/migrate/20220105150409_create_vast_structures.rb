class CreateVastStructures < ActiveRecord::Migration[7.0]
  def change
    create_table :vast_structures do |t|
      t.belongs_to :creative, null: false, foreign_key: true
      t.string :ad_title

      t.timestamps
    end
  end
end
