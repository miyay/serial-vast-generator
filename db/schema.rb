# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_02_13_061749) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "creatives", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "external_media_files", force: :cascade do |t|
    t.bigint "media_file_group_id", null: false
    t.string "external_host_url"
    t.string "mime_type"
    t.integer "width"
    t.integer "height"
    t.integer "bitrate"
    t.string "codec"
    t.integer "file_size"
    t.float "duration"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["media_file_group_id"], name: "index_external_media_files_on_media_file_group_id"
  end

  create_table "icon_resources", force: :cascade do |t|
    t.bigint "icon_structure_id", null: false
    t.string "format"
    t.string "icon_file"
    t.string "mime_type"
    t.integer "width"
    t.integer "height"
    t.jsonb "overlay_data", default: {}, null: false
    t.jsonb "qr_data", default: {}, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["icon_structure_id"], name: "index_icon_resources_on_icon_structure_id"
  end

  create_table "icon_structures", force: :cascade do |t|
    t.bigint "linear_structure_id", null: false
    t.string "program"
    t.string "x_position"
    t.string "y_position"
    t.integer "offset"
    t.integer "duration"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["linear_structure_id"], name: "index_icon_structures_on_linear_structure_id"
  end

  create_table "linear_structures", force: :cascade do |t|
    t.bigint "vast_structure_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["vast_structure_id"], name: "index_linear_structures_on_vast_structure_id"
  end

  create_table "media_file_groups", force: :cascade do |t|
    t.bigint "linear_structure_id", null: false
    t.float "duration"
    t.string "video_upload_format"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "original_media_file"
    t.jsonb "convert_option", default: {}, null: false
    t.string "overlay_image_file"
    t.index ["linear_structure_id"], name: "index_media_file_groups_on_linear_structure_id"
  end

  create_table "vast_structures", force: :cascade do |t|
    t.bigint "creative_id", null: false
    t.string "ad_title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["creative_id"], name: "index_vast_structures_on_creative_id"
  end

  add_foreign_key "external_media_files", "media_file_groups"
  add_foreign_key "icon_resources", "icon_structures"
  add_foreign_key "icon_structures", "linear_structures"
  add_foreign_key "linear_structures", "vast_structures"
  add_foreign_key "media_file_groups", "linear_structures"
  add_foreign_key "vast_structures", "creatives"
end
