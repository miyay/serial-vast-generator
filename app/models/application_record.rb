class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  private

  def hhmmss(sec)
    return "" unless sec
    Time.at(sec).utc.strftime("%T.%L")
  end
end
