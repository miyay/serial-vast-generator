class LinearStructure < ApplicationRecord
  belongs_to :vast_structure
  has_one :media_file_group, dependent: :destroy
  has_many :icon_structures, dependent: :destroy

  accepts_nested_attributes_for :media_file_group,
    allow_destroy: true
  accepts_nested_attributes_for :icon_structures,
    reject_if: :reject_icon_structures,
    allow_destroy: true

  def to_vast_xml(options = nil)
    <<-"XML".chomp
    <Linear>
      #{media_file_group.to_vast_xml(options)}
      <VideoClicks>
        <ClickThrough />
      </VideoClicks>
      #{build_icons(options) if icon_structures.present?}
    </Linear>
    XML
  end

  private

  def build_icons(options = {})
    <<-"XML".chomp
    <Icons>
      #{icon_structures.map{|is| is.to_vast_xml(options)}.join()}
    </Icons>
    XML
  end

  def reject_icon_structures(attributed)
    attributed["icon_resource_attributes"]["icon_file"].blank?
  end
end
