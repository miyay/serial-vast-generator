require 'open-uri'

class ExternalMediaFile < ApplicationRecord
  belongs_to :media_file_group

  before_validation :get_external_file_metadata

  validates :external_host_url,
    presence: true,
    format: /\A#{URI::regexp(%w(http https))}\z/

  def to_vast_xml
    <<-"XML".chomp
      <MediaFile delivery="#{::MediaFileGroup::DELIVERY[:progressive]}" type="#{mime_type}" bitrate="#{(bitrate / (2 ** 10).to_f).round(-2)}" width="#{width}" height="#{height}" scalable="0" codec="#{codec}">
        <![CDATA[#{external_host_url}]]>
      </MediaFile>
    XML
  end

  private

  def get_external_file_metadata
    return if self.external_host_url.blank?

    file_path = Rails.root.join('tmp', 'external_host_file', SecureRandom.uuid)

    Tempfile.open do |file|
      begin
        URI.open(self.external_host_url) do |res|
          IO.copy_stream(res, file)
        end

        self.mime_type = MimeMagic.by_magic(file)

        m = FFMPEG::Movie.new(file.path)
        self.duration  = m.duration
        self.width     = m.width
        self.height    = m.height
        self.bitrate   = m.bitrate
        self.codec     = m.video_codec
        self.file_size = m.size
      ensure
        file.close
        file.unlink
      end
    end
  rescue
    nil # TODO
  end
end
