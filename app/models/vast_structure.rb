require 'rexml/document'
require 'rexml/formatters/pretty'

class VastStructure < ApplicationRecord
  AD_SYSTEM = "SerialVastGenerator"

  belongs_to :creative
  has_many :linear_structures, dependent: :destroy

  accepts_nested_attributes_for :linear_structures

  def to_vast_xml(options = {})
    xml = <<-"XML"
<VAST version="4.2" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.iab.com/VAST">
  <Ad id="1">
    <InLine>
      <AdSystem version="1">#{AD_SYSTEM}</AdSystem>
      <Error><![CDATA[https://example.com/error]]></Error>
      <Impression id="Impression-ID"><![CDATA[https://example.com/track/impression]]></Impression>
      <Pricing model="cpm" currency="USD">
        <![CDATA[ 25.00 ]]>
      </Pricing>
      <AdServingId>00000000-0000-0000-0000-000000000000</AdServingId>
      <AdTitle>#{ad_title}</AdTitle>
      <Creatives>
#{creative_vast_xml(options)}
      </Creatives>
    </InLine>
  </Ad>
</VAST>
    XML

    output = StringIO.new
    ::REXML::Formatters::Pretty.new.write(REXML::Document.new(xml), output)
    output.string
  end

  private

  def creative_vast_xml(options = {})
    linear_structures.map.with_index do |ls, i|
      <<-"XML"
      <Creative id="#{i+1}" sequence="1" adId="1">
        #{ls.to_vast_xml(options)}
      </Creative>
      XML
    end.join("\n")
  end
end
