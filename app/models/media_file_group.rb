class MediaFileGroup < ApplicationRecord
  extend Enumerize

  DELIVERY = {
    progressive: "progressive",
    streaming:   "streaming"
  }.freeze

  # https://cloudinary.com/documentation/video_manipulation_and_delivery#video_codec_settings
  DEFAULT_CONVERT_SPEC = {
    low: {
      width:    640,
      height:   360,
      bitrate:  500,
      format:   "mp4",
      mimetype: "video/mp4",
      codec:    "h264",
      profile:  "baseline",
      level:    "3.0"
    },
    medium: {
      width:    1024,
      height:   576,
      bitrate:  1000,
      format:   "mp4",
      mimetype: "video/mp4",
      codec:    "h264",
      profile:  "baseline",
      level:    "3.0"
    },
    high: {
      width:    1920,
      height:   1080,
      bitrate:  3000,
      format:   "mp4",
      mimetype: "video/mp4",
      codec:    "h264",
      profile:  "high",
      level:    "4.0"
    }
  }.freeze

  belongs_to :linear_structure
  has_many :external_media_files, dependent: :destroy

  accepts_nested_attributes_for :external_media_files,
    allow_destroy: true

  attr_accessor :external_file_url_text

  attribute :external_file_url_text, :string, default: 'https://iab-publicfiles.s3.amazonaws.com/vast/VAST-4.0-Short-Intro.mp4'

  store :convert_option, accessors: %i(qr_url)

  enumerize :video_upload_format,
    in: [
      :external_host,
      :simple_video_upload,
      :custom_overlay_image,
      :qr_code_image
    ],
    default: :external_host

  mount_uploader :original_media_file, OriginalMediaFileUploader
  mount_uploader :overlay_image_file, OverlayImageFileUploader

  before_validation :set_external_media_files,
    if: -> { video_upload_format.external_host? }
  before_validation :set_original_media_metadata,
    unless: -> { video_upload_format.external_host? }    
  before_validation :create_qr_image,
    if: -> { video_upload_format.qr_code_image? }

  validates :video_upload_format,
    presence: true
  validates :qr_url,
    presence: true,
    if: -> { video_upload_format.qr_code_image? }
  validate :check_mf_duration

  def to_vast_xml(options = nil)
    case video_upload_format
    when "external_host"
      <<-"XML".chomp
      <Duration>#{duration}</Duration>
      <MediaFiles>
        #{external_media_files.map(&:to_vast_xml).join('\n')}
      </MediaFiles>
      XML
    when "simple_video_upload", "custom_overlay_image", "qr_code_image"
      xml = <<-"XML".chomp
      <Duration>#{duration}</Duration>
      <MediaFiles>
      XML

      self_convert_media_files.each do |mf|
        xml += <<-"XML".chomp
          <MediaFile delivery="#{::MediaFileGroup::DELIVERY[:progressive]}" type="#{mf[:mimetype]}" bitrate="#{mf[:bitrate]}" width="#{mf[:width]}" height="#{mf[:height]}" scalable="0" codec="#{mf[:codec]}">
            <![CDATA[#{mf[:url]}]]>
          </MediaFile>
        XML
      end

      xml += <<-"XML".chomp
      </MediaFiles>
      XML
    end
  end

  private

  def create_qr_image
    return if qr_url.blank?

    qr_png = RQRCode::QRCode.new(qr_url, level: :h).as_png

    tf = Tempfile.new("qr_image")
    tf.binmode
    tf << qr_png.to_s
    tf.rewind

    self.overlay_image_file = tf
  end

  def self_convert_media_files
    transformation_option =
      case video_upload_format
      when "custom_overlay_image", "qr_code_image"
        {
          overlay: overlay_image_file.file.public_id
        }
      else
        {}
      end

    DEFAULT_CONVERT_SPEC.values.map do |spec|
      url = ::Cloudinary::Utils.cloudinary_url(
        original_media_file,
        width:        spec[:width],
        height:       spec[:height],
        crop:         "scale",
        bit_rate:     "#{spec[:bitrate]}k",
        fetch_format: spec[:format],
        video_codec: {
          codec:   spec[:codec].dup, # dupしないとspec[:codec]が書き換わる
          profile: spec[:profile],
          level:   spec[:level]
        },
        resource_type: "video",
        transformation: transformation_option
      )

      {
        mimetype: spec[:mimetype],
        bitrate:  spec[:bitrate],
        width:    spec[:width],
        height:   spec[:height],
        codec:    spec[:codec],
        url:      url
      }
    end
  end

  def set_external_media_files
    external_file_url_text.split(/\R/).compact.each do |url|
      self.external_media_files.new(external_host_url: url)
    end
  end

  def set_original_media_metadata
    m = FFMPEG::Movie.new(original_media_file.file.path)
    self.duration = m.duration
  end

  def check_mf_duration
    return if external_media_files.blank?
    return unless external_media_files.all?(&:valid?)
    return unless external_media_files.first.duration

    d = external_media_files.first.duration.round(3)

    if external_media_files.all? {|emf| emf.duration.round(3) == d }
      self.duration = d
    end
  end
end
