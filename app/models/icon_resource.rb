class IconResource < ApplicationRecord
  extend Enumerize

  belongs_to :icon_structure

  store :overlay_data, accessors: %i(ov_default_text)
  store :qr_data, accessors: %i(qr_url)

  enumerize :format, in: [
    :simple_image,
    :text_overlay_image,
    :qr_code_image
  ]

  mount_uploader :icon_file, IconFileUploader

  before_validation :set_qr_icon, if: -> { format.qr_code_image? }
  before_validation :set_icon_metadata

  validates :icon_file,
    presence: true,
    if: -> { format.simple_image? || format.text_overlay_image? }

  validates :qr_url,
    presence: true,
    format: /\A#{URI::regexp(%w(http https))}\z/,
    if: -> { format.qr_code_image? }

  def to_vast_xml(options = {})
    <<-"XML".chomp
      <StaticResource creativeType="#{mime_type}">
        <![CDATA[#{icon_url(options)}]]>
      </StaticResource>
    XML
  end

  def icon_url(options = {})
    self.__send__("#{format}_icon_url", options)
  end

  private

  def set_icon_metadata
    return if icon_file.blank?
    self.mime_type = icon_file.content_type
    self.width, self.height = FastImage.size(icon_file.file.file)
  end

  def set_qr_icon
    qr_png = RQRCode::QRCode.new(qr_url, level: :h).as_png

    tf = Tempfile.new("qr_icon")
    tf.binmode
    tf << qr_png.to_s
    tf.rewind

    self.icon_file = tf
  end

  def simple_image_icon_url(_ = {})
    icon_file.to_s
  end

  def text_overlay_image_icon_url(options = {})
    ::Cloudinary::Utils.cloudinary_url(
      icon_file,
      overlay: {
        font_family: "Arial",
        font_size:   50,
        font_weight: "bold",
        text:        options[:it] || ov_default_text
      },
      color: "#B90FAE"
    )
  end

  def qr_code_image_icon_url(_ = {})
    icon_file.to_s
  end
end
