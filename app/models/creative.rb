class Creative < ApplicationRecord
  has_one :vast_structure, dependent: :destroy

  accepts_nested_attributes_for :vast_structure

  validates :title, presence: true
end
