class IconStructure < ApplicationRecord
  belongs_to :linear_structure
  has_one :icon_resource, dependent: :destroy

  accepts_nested_attributes_for :icon_resource, allow_destroy: true

  after_save :set_default_data

  def to_vast_xml(options = {})
    <<-"XML".chomp
      <Icon program="#{program}" height="#{icon_resource.height}" width="#{icon_resource.width}" xPosition="#{x_position}" yPosition="#{y_position}" duration="#{hhmmss(duration)}" offset="#{hhmmss(offset)}">
        #{icon_resource.to_vast_xml(options)}
        <IconClicks>
          <IconClickThrough />
        </IconClicks>
      </Icon>
    XML
  end

  private

  def set_default_data
    if program.blank?
      update_column(:program, "icon_#{id}")
    end
  end
end
