class CreativesController < ApplicationController
  before_action :set_creative, only: %i[ show edit update destroy vast_xml ]

  # GET /creatives or /creatives.json
  def index
    @creatives = Creative.all
  end

  # GET /creatives/1 or /creatives/1.json
  def show
  end

  # GET /creatives/new
  def new
    @creative = Creative.new
    @creative.build_vast_structure
    @creative.vast_structure.linear_structures.build
    @creative.vast_structure.linear_structures.first.build_media_file_group
    3.times do
      is = @creative.vast_structure.linear_structures.first.icon_structures.build
      is.build_icon_resource
    end
  end

  # GET /creatives/1/edit
  def edit
  end

  # POST /creatives or /creatives.json
  def create
    @creative = Creative.new(creative_params)

    respond_to do |format|
      if @creative.save
        format.html { redirect_to creative_url(@creative), notice: "Creative was successfully created." }
        format.json { render :show, status: :created, location: @creative }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @creative.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /creatives/1 or /creatives/1.json
  def update
    respond_to do |format|
      if @creative.update(creative_params)
        format.html { redirect_to creative_url(@creative), notice: "Creative was successfully updated." }
        format.json { render :show, status: :ok, location: @creative }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @creative.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /creatives/1 or /creatives/1.json
  def destroy
    @creative.destroy

    respond_to do |format|
      format.html { redirect_to creatives_url, notice: "Creative was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def vast_xml
    options = {it: params[:it]}
    render xml: @creative.vast_structure.to_vast_xml(options)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_creative
      @creative = Creative.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def creative_params
      params.require(:creative).permit(
        :title,
        vast_structure_attributes: [
          :id,
          :ad_title,
          linear_structures_attributes: [
            :id,
            :_destroy,
            media_file_group_attributes: [
              :id,
              :_destroy,
              :video_upload_format,
              :external_file_url_text,
              :original_media_file,
              :overlay_image_file,
              :qr_url
            ],
            icon_structures_attributes: [
              :id,
              :program,
              :x_position,
              :y_position,
              :offset,
              :duration,
              :_destroy,
              icon_resource_attributes: [
                :id,
                :_destroy,
                :format,
                :icon_file,
                :ov_default_text,
                :qr_url
              ]
            ]
          ]
        ]
      )
    end
end
