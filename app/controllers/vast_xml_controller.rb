class VastXmlController < ApplicationController
  def sample_icon_base_text
    media_files = [
      {
        url:                   "https://iab-publicfiles.s3.amazonaws.com/vast/VAST-4.0-Short-Intro.mp4",
        delivery:              "progressive",
        type:                  "video/mp4",
        bitrate:               "2000",
        min_bitrate:           "1500",
        max_bitrate:           "2500",
        width:                 "1280",
        height:                "720",
        scalable:              "1",
        maintain_aspect_ratio: "1",
        codec:                 "H.264"
      }
    ]

    itext = params[:text] || "動的文字生成"
    icon_url = ::Cloudinary::Utils.cloudinary_url(
      "frame_hibiscus_yoko_pflqtb",
      overlay: {
        font_family: "Arial",
        font_size:   50,
        font_weight: "bold",
        text:        itext
      },
      color: "#B90FAE"
    )

    icons = [
      {
        program:    "freetext",
        url:        icon_url,
        type:       "image/jpeg",
        width:      300,
        height:     203,
        x_position: 0,
        y_position: "top"
      }
    ]

    hash = {
      duration:    "00:00:16",
      media_files: media_files,
      icons:       icons,
      ad_title:    "icon sample"
    }

    render xml: VastBuilder.new(hash).build
  end

  def sample_video_base_text
    itext = params[:text] || "動的文字生成"

    # https://cloudinary.com/documentation/video_manipulation_and_delivery#apply_video_transparency
    video_url = ::Cloudinary::Utils.cloudinary_url(
      "sample_low_rutfdq",
      transformation: [
        {
          overlay: "hibiscus_japa0x"
        },
        {
          width: 200,
          crop: "scale"
        },
        {
          start_offset: "1.0",
          end_offset: "6.0",
          flags: "layer_apply",
          gravity: "south",
          y: 60
        },
        {
          overlay: {
            font_family:"arial",
            font_size: 40,
            text: itext
          }
        },
        {
          start_offset: "1.0",
          end_offset: "6.0",
          flags: "layer_apply",
          gravity: "south",
          y: 100
        }
      ],
      resource_type: "video"
    )

    media_files = [
      {
        url:                   video_url,
        delivery:              "progressive",
        type:                  "video/mp4",
        bitrate:               "1170",
        min_bitrate:           "1170",
        max_bitrate:           "1170",
        width:                 "950",
        height:                "540",
        scalable:              "1",
        maintain_aspect_ratio: "1",
        codec:                 "H.264"
      }
    ]

    hash = {
      duration:    "00:00:16",
      media_files: media_files,
      ad_title:    "icon sample"
    }

    render xml: VastBuilder.new(hash).build
  end
end
